//
//  ProfileCoordinatorView.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 5/17/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

protocol StackCoordinatorProtocol: AlertSenderProtocol, ProvidesFooterComponent {
    var title: String { get }
    var components: [UIViewController] { get }
    var footerHeight: CGFloat { get }
}

class StackCoordinatorViewController: UIViewController, AlertShowerProtocol, HasFooterViewProtocol {

    var coordinator: StackCoordinatorProtocol?
    
    // # MARK: IBOutlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!

    // # MARK: HasFooterViewProtocol
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = coordinator?.title
        
        if let coordinator = coordinator {
            attachAlertSender(alertSender: coordinator)
            addFooter(footerProvider: coordinator)
        }        
    }
    
    override func viewDidLayoutSubviews() {
        coordinator?.components.forEach { stackable in
            self.addChildViewController(stackable)
            stackView.addArrangedSubview(stackable.view)
        }
        
        super.viewDidLayoutSubviews()
    }
}
