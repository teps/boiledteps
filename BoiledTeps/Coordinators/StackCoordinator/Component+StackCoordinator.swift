//
//  Component+Coordinator.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 5/28/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

extension Components {
    static func stackCoordinator(coordinator: StackCoordinatorProtocol) -> UIViewController {
        
        guard let stackCoordinatorViewController = viewController(storyboardFile: "StackCoordinator") as? StackCoordinatorViewController else {
            fatalError("Could not create Coordinator")
        }
        
        stackCoordinatorViewController.coordinator = coordinator
        
        return stackCoordinatorViewController
    }
}
