//
//  TableViewCoordinatorViewController.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/24/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit
import DZNEmptyDataSet

typealias TableViewCoordinatorView = UITableViewHeaderFooterView & TableViewCoordinatorViewProtocol
typealias TableViewCoordinatorCell = UITableViewCell & TableViewCoordinatorCellProtocol

// Section and Footer view
protocol ViewModelProtocol {
    static var viewIdentifier: String { get }
}

protocol TableViewCoordinatorViewProtocol {
    var viewModel: ViewModelProtocol? { get set }
    var style: Any? { get set }
}

// Cell views
protocol CellViewModelProtocol {
    static var cellIdentifier: String { get }
}

protocol TableViewCoordinatorCellProtocol {
    var cellViewModel: CellViewModelProtocol? { get set }
    var style: Any? { get set }
}

// Empty data classes
class DnzEmptyDataStyle {
    var image: UIImage? = nil
    var titleFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
    var descriptionFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
    var buttonTitleFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.callout)
}

protocol DnzEmptyDataViewModelProtocol {
    var title: String? { get }
    var description: String? { get }
    var buttonTitle: String? { get }
    
    var didTapRetry: SafePublishSubject<Void> { get }
}

fileprivate extension DnzEmptyDataViewModelProtocol {
    func buttonTapped() {
        didTapRetry.next()
    }
}

// Coordinator view model
protocol TableViewCoordinatorViewModelProtocol: LoadingScreenSenderProtocol, ProvidesFooterComponent {
    var title: String { get }
    var numberOfSections: Int { get }
    var cellsToRegister: [(nibName: String, reuseIdentifier: String)] { get }
    var headerFooterToRegister: [(nibName: String, reuseIdentifier: String)] { get }
    var contentDidUpdate: SafePublishSubject<Void> { get }
    var dnzEmptyDataViewModel: DnzEmptyDataViewModelProtocol? { get }
    var footer: UIViewController? { get }

    func viewDidAppear()
    func didSelectIndexPath(indexPath: IndexPath)
    func sectionViewModelFor(section: Int) -> ViewModelProtocol?
    func cellViewModelFor(indexPath: IndexPath) -> CellViewModelProtocol?
    func numberOfRowsInSection(section: Int) -> Int
}

extension TableViewCoordinatorViewModelProtocol {
    func viewDidAppear() { }
    func sectionViewModelFor(section: Int) -> ViewModelProtocol? { return nil }
}

class TableViewCoordinatorViewControllerStyle {
    var dnzEmptyDataStyle = DnzEmptyDataStyle()
}

class TableViewCoordinatorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LoadingScreenProtocol, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, HasFooterViewProtocol {
    
    var coordinator: TableViewCoordinatorViewModelProtocol?
    var style = TableViewCoordinatorViewControllerStyle()
    
    // # MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // # MARK: HasFooterViewProtocol
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 80.0
        
        self.footerHeight.constant = coordinator?.footer == nil ? 0.0 : 80.0
        
        // DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self

        guard let coordinator = coordinator else {
            fatalError("Coordinator not allocated")
        }
        
        title = coordinator.title

        attachShowLoadingScreenViewModel(sender: coordinator, view: tableView)
        addFooter(footerProvider: coordinator)
        
        for registerCell in self.coordinator?.cellsToRegister ?? [] {
            tableView.register(UINib(nibName: registerCell.nibName, bundle: nil), forCellReuseIdentifier: registerCell.reuseIdentifier)
        }
        
        for registerView in self.coordinator?.headerFooterToRegister ?? [] {
            tableView.register(UINib(nibName: registerView.nibName, bundle: nil), forHeaderFooterViewReuseIdentifier: registerView.reuseIdentifier)
        }
        
        tableView.tableFooterView = UIView()
        
        coordinator.contentDidUpdate.observeNext { [weak self] in
            self?.tableView.reloadData()
        }.dispose(in: bag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        coordinator?.viewDidAppear()
    }
    
    // # MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        coordinator?.didSelectIndexPath(indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coordinator?.numberOfRowsInSection(section: section) ?? 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return coordinator?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let viewModel = coordinator?.sectionViewModelFor(section: section),
              var view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MonthSectionHeaderView") as? TableViewCoordinatorView else {
            return nil
        }
        
        view.viewModel = viewModel

        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cellViewModel = coordinator?.cellViewModelFor(indexPath: indexPath),
            var cell = tableView.dequeueReusableCell(withIdentifier: type(of: cellViewModel).cellIdentifier) as? TableViewCoordinatorCell,
            let coordinator = coordinator else {
                fatalError("Could not return cell")
        }

        cell.cellViewModel = coordinator.cellViewModelFor(indexPath: indexPath)
        
        return cell
    }
    
    // # MARK: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {

        guard let title = coordinator?.dnzEmptyDataViewModel?.title, !isLoading() else {
            return nil
        }

        let attrs = [NSAttributedStringKey.font: style.dnzEmptyDataStyle.titleFont]
        return NSAttributedString(string: title, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {

        guard let description = coordinator?.dnzEmptyDataViewModel?.description, !isLoading() else {
            return nil
        }

        let attrs = [NSAttributedStringKey.font: style.dnzEmptyDataStyle.descriptionFont]
        return NSAttributedString(string: description, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return self.style.dnzEmptyDataStyle.image
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControlState) -> NSAttributedString? {

        guard let buttonTitle = coordinator?.dnzEmptyDataViewModel?.buttonTitle, !isLoading() else {
            return nil
        }

        let attrs = [NSAttributedStringKey.font: style.dnzEmptyDataStyle.buttonTitleFont]
        return NSAttributedString(string: buttonTitle, attributes: attrs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        coordinator?.dnzEmptyDataViewModel?.buttonTapped()
    }
    
    // # MARK: Private Helper methods
    private func isLoading() -> Bool {
        if coordinator?.loadingScreen.value ?? true {
            return true
        }
        return false
    }
}
