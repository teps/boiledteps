//
//  HasFooterViewProtocol.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/3/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

protocol ProvidesFooterComponent {
    var footer: UIViewController? { get }
    var footerHeight: CGFloat { get }
}

extension ProvidesFooterComponent {
    var footerHeight: CGFloat {
        return 0
    }
}


protocol HasFooterViewProtocol {
    
    var footerView: UIView! { get }
    var footerHeight: NSLayoutConstraint! { get }
}


extension HasFooterViewProtocol where Self: UIViewController {
    func addFooter(footerProvider: ProvidesFooterComponent) {
        if let footer = footerProvider.footer {
            // Add the footer view
            footer.view.frame = CGRect(x: 0, y: 0, width: footerView.frame.width, height: footerView.frame.height)
            self.addChildViewController(footer)
            footerView.addSubview(footer.view)
            
            self.footerHeight.constant = footerProvider.footerHeight
        }
        else {
            self.footerHeight.constant = 0
        }
    }
}
