//
//  EmptyPlatformDataViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/1/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class EmptyPlatformDataViewModel: DnzEmptyDataViewModelProtocol {

    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "Platforms"
    var description: String? = "No platforms found"
    var buttonTitle: String? = nil
    var didTapRetry = SafePublishSubject<Void>()
}
