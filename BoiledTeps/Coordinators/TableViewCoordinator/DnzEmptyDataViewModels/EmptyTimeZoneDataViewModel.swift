//
//  EmptyTimeZoneDataViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/1/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class EmptyTimeZoneDataViewModel: DnzEmptyDataViewModelProtocol {

    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "Time Zones"
    var description: String? = "No time zones found"
    var buttonTitle: String? = nil
    var didTapRetry = SafePublishSubject<Void>()
}
