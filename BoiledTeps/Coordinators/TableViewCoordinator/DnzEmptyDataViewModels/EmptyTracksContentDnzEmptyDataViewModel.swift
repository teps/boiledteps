//
//  EmptyTracksContentDnzEmptyDataViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/1/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class EmptyTracksContentDnzEmptyDataViewModel: DnzEmptyDataViewModelProtocol {

    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "Tracks"
    var description: String? = "No tracks found"
    var buttonTitle: String? = nil
    var didTapRetry = SafePublishSubject<Void>()
}
