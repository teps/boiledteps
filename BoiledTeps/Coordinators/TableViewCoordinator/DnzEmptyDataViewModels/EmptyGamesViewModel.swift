//
//  EmptyGamesViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/1/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class EmptyGamesViewModel: DnzEmptyDataViewModelProtocol {

    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "Games"
    var description: String? = "No games found"
    var buttonTitle: String? = nil
    var didTapRetry = SafePublishSubject<Void>()
}
