//
//  ConnectionErrorEmptyDataViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/3/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class ConnectionErrorEmptyDataViewModel: DnzEmptyDataViewModelProtocol {
    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "Error"
    var description: String? = "Connection error"
    var buttonTitle: String? = "Retry"
    var didTapRetry = SafePublishSubject<Void>()
}
