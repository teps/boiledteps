//
//  EmptyRacesContentDnzEmptyDataViewModel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/4/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class EmptyRacesContentDnzEmptyDataViewModel: DnzEmptyDataViewModelProtocol {
    // # MARK: DnzEmptyDataViewModelProtocol
    var title: String? = "No Races Found"
    var description: String? = "We did not find any races at this time."
    var buttonTitle: String? = nil
    var didTapRetry = SafePublishSubject<Void>()
}
