//
//  Component+TableViewCoordinator.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/24/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

extension Components {

    static func tableViewCoordinator(coordinator: TableViewCoordinatorViewModelProtocol) -> UIViewController {
        guard let tableViewCoordinatorViewController = viewController(storyboardFile: "TableViewCoordinator") as? TableViewCoordinatorViewController else {
            fatalError("Could not create Coordinator")
        }
        
        tableViewCoordinatorViewController.coordinator = coordinator
        
        return tableViewCoordinatorViewController
    }
}
