//
//  JenStyle+UIView.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/7/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

class UIViewStyle {
    
    var backgroundColor: UIColor = .white
    var borderWidth: CGFloat = 0
    var borderColor: UIColor = .clear
    var shadowOpacity: Float = 0.0
    var shadowRadius: CGFloat = 0.0
    var shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0)

    init() { }
}

extension UIView {

    func applyStyle(style: UIViewStyle) {
        backgroundColor = style.backgroundColor
        self.layer.borderWidth = style.borderWidth
        self.layer.borderColor = style.borderColor.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowOffset = style.shadowOffset
        self.layer.shadowRadius = style.shadowRadius
        self.layer.shadowOpacity = style.shadowOpacity
    }
}
