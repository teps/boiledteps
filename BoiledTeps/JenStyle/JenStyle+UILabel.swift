//
//  JenStyle+UILabel.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/24/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class UILabelStyle: UIViewStyle {

    var textColor = UIColor.black
    var textAlignment = NSTextAlignment.left
    
    override init() {
        super.init()
    }
}

extension UILabel {
    
    func applyStyle(style: UILabelStyle) {
        super.applyStyle(style: style)

        self.textColor = style.textColor
        self.textAlignment = style.textAlignment
    }
}
