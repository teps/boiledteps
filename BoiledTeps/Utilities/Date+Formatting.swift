//
//  Date+Formatting.swift
//  SportPals_iOS
//
//  Created by Terrick Mansur on 4/25/17.
//  Copyright © 2017 Terrick Mansur. All rights reserved.
//

import UIKit

extension Date {

    func nearest15Min() -> Date {
        let calendar = Calendar.current
        let interval = 15
        let nextDiff = interval - calendar.component(.minute, from: self) % interval
        let nextDate = calendar.date(byAdding: .minute, value: nextDiff, to: self) ?? Date()
        return nextDate
    }
    
    func readable(timeZone: TimeZone = TimeZone.current) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.timeZone = timeZone
        return dateFormatter.string(from: self)
    }
    
    func truncateSecondsForDate() -> Date {
        
        let calendar = Calendar.current
        let fromDateComponents: DateComponents = calendar.dateComponents([.era , .year , .month , .day , .hour , .minute], from: self) as DateComponents
        
        return calendar.date(from: fromDateComponents as DateComponents)! as Date
    }

    var readableDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        return dateFormatter.string(from: self)
    }

    var readableDayOfTheWeek: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
    
    var readableMonth: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
    
    var readableDayOfTheMonth: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "M"
        return dateFormatter.string(from: self)
    }

    var readableMonthAndDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d"
        return dateFormatter.string(from: self)
    }
    
    var readableYear: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "Y"
        return dateFormatter.string(from: self)
    }
    
    var readableTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self)
    }

}
