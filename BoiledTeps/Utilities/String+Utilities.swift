//
//  String+Utilities.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/9/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
