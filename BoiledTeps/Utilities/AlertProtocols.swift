//
//  AlertProtocols.swift
//  SportPals_iOS
//
//  Created by Terrick Mansur on 9/17/17.
//  Copyright © 2017 Terrick Mansur. All rights reserved.
//

import UIKit
import ReactiveKit

typealias Alert = (title: String, message: String, style: UIAlertControllerStyle)

protocol AlertSenderProtocol {
    var sendAlert: SafePublishSubject<Alert> { get }
}

protocol AlertShowerProtocol {
    var bag: DisposeBag { get }
    func attachAlertSender(alertSender: AlertSenderProtocol)
}

extension AlertShowerProtocol where Self: UIViewController {
    func attachAlertSender(alertSender: AlertSenderProtocol) {
        alertSender.sendAlert.observeNext { title, message, style in
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: style)
            alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertViewController, animated: true, completion: nil)
        }.dispose(in: self.bag)
    }
}

extension AlertShowerProtocol where Self: FlowController {
    func attachAlertSender(alertSender: AlertSenderProtocol) {
        alertSender.sendAlert.observeNext { title, message, style in
            let alertViewController = UIAlertController(title: title, message: message, preferredStyle: style)
            alertViewController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.root.present(alertViewController, animated: true, completion: nil)
        }.dispose(in: self.bag)
    }
}



