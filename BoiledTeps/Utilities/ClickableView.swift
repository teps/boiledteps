//
//  ClickableView.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/24/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit

class ClickableUIViewStyle {

    var normalStateStyle = UIViewStyle()
    var pressedState = UIViewStyle()

    init() { }
}

class ClickableUIView: UIView {

    var didClick = SafePublishSubject<Void>()
    
    // # MARK: Privates
    private var style = ClickableUIViewStyle()

    func applyStyle(style: ClickableUIViewStyle) {
        self.style = style
        super.applyStyle(style: style.normalStateStyle)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        // Animate
        super.applyStyle(style: self.style.pressedState)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        // Animate
        UIView.animate(withDuration: 0.08, animations: {
            super.applyStyle(style: self.style.normalStateStyle)
        })
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        UIView.animate(withDuration: 0.08, animations: {
            super.applyStyle(style: self.style.normalStateStyle)
        })
        self.didClick.next()
    }
}
