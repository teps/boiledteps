//
//  LoadingScreenProtocol.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 7/1/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit
import ReactiveKit
import SnapKit

protocol LoadingScreenProtocol { }

extension LoadingScreenProtocol where Self: UIViewController {
    func attachShowLoadingScreenViewModel(sender: LoadingScreenSenderProtocol, view: UIView) {
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        indicator.hidesWhenStopped = true
        indicator.color = .gray
        
        self.view.addSubview(indicator)
        self.view.bringSubview(toFront: indicator)
        
        indicator.snp.makeConstraints { make in
            make.top.equalTo(view)
            make.trailing.equalTo(view)
            make.bottom.equalTo(view)
            make.leading.equalTo(view)
        }
        
        sender.loadingScreen.observeNext { [weak indicator] loading in
            loading ? indicator?.startAnimating() : indicator?.stopAnimating()
            }.dispose(in: self.bag)
    }
}
protocol LoadingScreenSenderProtocol {
    var loadingScreen: Property<Bool> { get }
}
