//
//  UIViewController+Utilities.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 6/24/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

extension UIViewController {
    func inNavController(leftButton: UIBarButtonItem? = nil, rigthButton: UIBarButtonItem? = nil) -> UINavigationController {
        let navCon = UINavigationController(rootViewController: self)

        self.navigationItem.setLeftBarButton(leftButton, animated: false)
        self.navigationItem.setRightBarButton(rigthButton, animated: false)

        return navCon
    }    
}
