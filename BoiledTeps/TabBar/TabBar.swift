//
//  TabBar.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 8/12/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

public protocol TabBarController {
    var tabs: [UIViewController] { get }
}

open class TabBar: UITabBarController {
    
    var controller: TabBarController! = nil
        
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewControllers = controller.tabs
        for viewContoller in self.viewControllers! {
            viewContoller.view.layoutIfNeeded()
        }
    }
}
