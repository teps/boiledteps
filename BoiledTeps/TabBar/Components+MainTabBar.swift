//
//  Components+MainTabBar.swift
//  eRaceLeague-iOS
//
//  Created by Terrick Mansur on 4/14/18.
//  Copyright © 2018 Arubook N.V. All rights reserved.
//

import UIKit

extension Components {

    open static func tabBar(controller: TabBarController) -> UITabBarController {
        
        guard let mainTabBar: TabBar = UIStoryboard.init(name: "TabBar", bundle: Bundle.main).instantiateViewController(withIdentifier: "TabBar") as? TabBar else {
            fatalError("Could not create view controller.")
        }
        
        mainTabBar.controller = controller

        return mainTabBar
    }
}
