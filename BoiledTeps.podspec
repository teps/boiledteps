Pod::Spec.new do |s|

	s.platform     = :ios, "9.0"
  	s.name         	= "BoiledTeps"
  	s.version      	= "1.0.0"
  	s.summary     	= "Boiler plate code I always use."
  	s.description 	= "This is the framework I include in all my project."
  	s.homepage    	= "http://arubook.com/"
  	s.license     	= "MIT"
  	s.author        = { "Terrick Mansur" => "terrickmansur@gmail.com" }

  	s.source 		= { :git => "https://teps@bitbucket.org/teps/arubook.git", :tag => "#{s.version}" }
  	s.source_files  = "BoiledTeps/**/*.{h,m,swift}"
	s.dependency 'ReactiveKit'
	s.dependency 'Bond'
	s.dependency 'DZNEmptyDataSet'
    s.dependency 'SnapKit', '~> 4.0.0'

end
